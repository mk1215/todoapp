import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Row, Col } from "react-bootstrap";
import TodoTask from "./TodoTask";
import { API } from "aws-amplify";
import * as mutations from "../graphql/mutations";
import NewTodoForm from "./NewTodoForm";
import CustomModal from "./CustomModal";
import { getLocalDate } from "./Todo.js";

const capitalize = (str) =>
  str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);

const TodoCard = ({ todoItems, fetchTodos }) => {
  const [isAddFormOpen, setAddFormOpen] = useState(null);
  const [updateID, setUpdateID] = useState(null);

  const deleteTodo = async (ID) => {
    const idDetails = {
      id: ID,
    };
    const deleteSingleTodo = await API.graphql({
      query: mutations?.deleteTodo,
      variables: { input: idDetails },
    });
    fetchTodos();
  };

  const updateTodo = (id) => {
    console.log(id);
    setAddFormOpen(true);
    setUpdateID(id);
  };

  const handleCloseModal = () => {
    setUpdateID(null);
    setAddFormOpen(false);
  };

  return (
    <div className="m-3">
      <Card style={{ width: "13rem", backgroundColor: "#a9c8ff" }}>
        <h6 style={{ color: "#363636", fontSize: " 0.8rem", margin: "6px" }}>
          Updated at: {getLocalDate(todoItems.updatedAt)}
        </h6>
        <Card.Img
          variant="top"
          src="https://i.pinimg.com/originals/5e/81/9a/5e819a5ce865476b73087fd1276e7c3e.png"
        />

        <Card.Body>
          <Card.Title> {capitalize(todoItems.name)}</Card.Title>

          <Card.Text>{todoItems.description}</Card.Text>
          <Row>
            <Col md={6}>
              <Button
                variant="success"
                onClick={() => {
                  updateTodo(todoItems);
                }}
              >
                Update
              </Button>
            </Col>
            <Col md={6}>
              <Button
                variant="danger"
                onClick={() => {
                  deleteTodo(todoItems?.id);
                }}
              >
                Delete
              </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      {isAddFormOpen ? (
        <CustomModal
          showModal={true}
          children={
            <NewTodoForm
              handleCloseModal={handleCloseModal}
              fetchTodos={fetchTodos}
              id={updateID}
            />
          }
        />
      ) : null}
    </div>
  );
};
export default TodoCard;
