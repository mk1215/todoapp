import React from 'react';
import { Modal } from 'react-bootstrap';

const CustomModal = ({ children, showModal }) => {
  return (
    <Modal size='lg' centered show={showModal}>
      <Modal.Body className='text-center'>
        {children}
      </Modal.Body>
    </Modal>
  );
};

export default CustomModal;
