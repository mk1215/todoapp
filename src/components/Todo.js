import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import TodoTask from "./TodoTask";
import { Amplify, API, graphqlOperation } from "aws-amplify";

export const getLocalDate = (date) => {
  return date
    ? new Date(date).toLocaleDateString("en-us", {
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
      })
    : new Date().toLocaleDateString("en-us", {
        weekday: "long",
        month: "long",
        day: "numeric",
      });
};

const Todo = () => {
    const [todoList, setTodoList] = useState([]);
    const fetchTodos = async () => {
        const getTodos = await API.graphql({
          query: `query ListTodos(
              $filter: ModelTodoFilterInput
              $limit: Int
              $nextToken: String
            ) {
              listTodos(filter: $filter, limit: $limit, nextToken: $nextToken) {
                items {
                  id
                  name
                  description
                  createdAt
                  updatedAt
                }
                nextToken
              }
            }
          `,
        });
        setTodoList(getTodos?.data?.listTodos?.items);
        
      };
    
      useState(async () => {
        await fetchTodos();
      }, []);

      const NoToDos = () =>  <>
      <h1>Today</h1>
      <div>
      <p>{getLocalDate()}</p>
      </div>
      <TodoTask fetchTodos={fetchTodos} todoList={todoList}/>
      <img style={{ width: '49%', height: '49%', }} src="https://cdn.dribbble.com/users/846207/screenshots/11359927/loading-animation_sand-timer.gif"/>
      </>

  return (
    <div
      className={`${todoList?.length ? 'd-flex' : 'd-block'} justify-content-center m-3`}
      style={ todoList?.length ? { background: "#fbfbfb" } : { opacity: 1}}
    >
       { todoList?.length ?
       <Card style={{ width: "auto" }}>
       <h1>Today</h1>
       <p>{getLocalDate()}</p>
       <TodoTask fetchTodos={fetchTodos} todoList={todoList}/>
     </Card>
     :
       <><NoToDos />
       </>
       }
      
    </div>
  );
};
export default Todo;
