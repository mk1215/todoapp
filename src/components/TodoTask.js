import React, { useState } from "react";
import { Amplify, API, graphqlOperation } from "aws-amplify";

import TodoCard from "./TodoCard";
import NewTodoForm from "./NewTodoForm";
import CustomModal from "./CustomModal";
import Button from "react-bootstrap/Button";
import { Row, Col, Form } from "react-bootstrap";

const TodoTask = ({ fetchTodos, todoList }) => {

  const [isAddFormOpen, setAddFormOpen] = useState(null);


  const handleCloseModal = (val) => {
    setAddFormOpen(val);
  };

  return (
    <>
      <Row className="justify-content-center">
        <Col md={12}>
          <Button
            variant="primary"
            className="text-decoration-none text-center"
            onClick={(e) => {
              setAddFormOpen(true);
            }}
          >
            Add New ToDo
          </Button>
        </Col>
      </Row>
      {isAddFormOpen ? (
        <CustomModal
          showModal={true}
          children={
            <NewTodoForm
              handleCloseModal={handleCloseModal}
              fetchTodos={fetchTodos}
            />
          }
        />
      ) : null}

{todoList?.length ?
      <div className="d-flex justify-content-center m-4 flex-wrap">
        {todoList?.map((item) => (
          <TodoCard todoItems={item} fetchTodos={fetchTodos} />
        ))}
      </div>
      : null }
    </>
  );
};

export default TodoTask;
