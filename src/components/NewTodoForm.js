import React, { Fragment, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import { Row, Col, Form } from "react-bootstrap";
import TextInput from "./TextInput.js";
import { API } from "aws-amplify";
import * as mutations from "../graphql/mutations";

const NewTodoForm = ({
  fetchTodos,
  handleCloseModal,
  id = null,
  handleUpdateID = () => {},
}) => {
  const [todoName, setTodoName] = useState(null);
  const [todoDescription, setTodoDescription] = useState(null);

  const handleData = ({ target }) => {
    if (target.name === "todoName") {
      setTodoName(target?.value);
    }
    if (target.name === "todoDescription") {
      setTodoDescription(target?.value);
    }
  };

  const handleContinue = async (e) => {
    e.preventDefault();

    const todoUpdateDetails = {
      description: todoDescription,
      name: todoName,
      id: id?.id,
    };
    const todoDetails = {
      description: todoDescription,
      name: todoName,
    };

    const saveTodo = await API.graphql({
      query: id ? mutations?.updateTodo : mutations?.createTodo,
      variables: { input: id ? todoUpdateDetails : todoDetails },
    });
    fetchTodos();
    handleCloseModal(false);
    handleUpdateID(null);
  };

  useEffect(() => {
    debugger;
    if (id) {
      setTodoName(id?.name);
      setTodoDescription(id?.description);
    }
  }, []);
  return (
    <div className="m-3">
      <Form onSubmit={handleContinue}>
        <Row>
          <Col md={12}>
            <TextInput
              label="Todo Name"
              name="todoName"
              value={todoName}
              onChange={(e) => handleData(e)}
              placeholder="Enter Name"
              isInvalid={todoName?.length ? false : true}
              isInfoBubbleAvailable={false}
            />
          </Col>
          <Col md={12}>
            <TextInput
              label="Todo Description"
              name="todoDescription"
              value={todoDescription}
              onChange={(e) => handleData(e)}
              placeholder="Enter Description"
              isInvalid={todoDescription?.length ? false : true}
              isInfoBubbleAvailable={false}
            />
          </Col>
        </Row>
        <Button
          type="submit"
          variant="primary"
          className="text-decoration-none text-center"
        >
          Submit
        </Button>
        <Button
          type="button"
          variant="outline-secondary"
          className="mx-4 text-decoration-none text-center"
          onClick={(e) => {
            handleCloseModal(false);
          }}
        >
          {" "}
          Cancel{" "}
        </Button>
      </Form>
    </div>
  );
};
export default NewTodoForm;
