export type AmplifyDependentResourcesAttributes = {
  "api": {
    "basictodo": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string",
      "GraphQLAPIKeyOutput": "string"
    }
  }
}